#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARR_WIDTH 30000

typedef char Cell;

enum Command
{
	CMD_INC_PTR = '>',
	CMD_DEC_PTR = '<',
	CMD_INC_BYT = '+',
	CMD_DEC_BYT = '-',
	CMD_OUT_BYT = '.',
	CMD_IN_BYT = ',',
	CMD_LOOP_BEG = '[',
	CMD_LOOP_END = ']'
};

// Example programs
// ++++++ [ > ++++++++++ < - ] > +++++ .
// ,[>+<-]>.
// ,>,< [ > [ >+ >+ << -] >> [- << + >>] <<< -] >> .
// ++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.
const char* program =
		"++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.";

int main()
{
	Cell cells[ARR_WIDTH];
	memset(cells, 0, ARR_WIDTH);
	int cell_ptr = 0;
	int loop_scope = 0;

	for (int i = 0; i < strlen(program); i++)
	{
		switch (program[i])
		{
			case CMD_INC_PTR: // Increment the data pointer by one
			{
				if (++cell_ptr == ARR_WIDTH)
				{
					cell_ptr = 0;
				}
			}
			break;
			case CMD_DEC_PTR: // Increment the data pointer by one
			{
				if (--cell_ptr < 0)
				{
					cell_ptr = ARR_WIDTH - 1;
				}
			}
			break;
			case CMD_INC_BYT: // Increment the byte at the data pointer by one
				cells[cell_ptr]++;
				break;
			case CMD_DEC_BYT: // Decrement the byte at the data pointer by one
				cells[cell_ptr]--;
				break;
			case CMD_OUT_BYT: // Output the byte at the data pointer
			{
				printf("%c", cells[cell_ptr]);
				// TODO Below works for multipication code i.e. printing integers, need to check for integers to decide which of these to use
				/* 	char out; */
				/* 	sprintf(&out, "%d", cells[cell_ptr]); */
				/* 	printf("%c", out); */
			}
			break;
			case CMD_IN_BYT: // Accept one byte of input, storing its value in the byte at the data pointer
				scanf("%hhd", (Cell*) &cells[cell_ptr]);
				//				cells[cell_ptr] = (Cell) getchar();
				break;
			case CMD_LOOP_BEG: // If the byte at the data pointer is zero, jump to the corresponding brace ]
				if (cells[cell_ptr] == 0)
				{
					do
					{
						if (program[i] == CMD_LOOP_BEG)
						{
							loop_scope++;
						}
						else if (program[i] == CMD_LOOP_END)
						{
							loop_scope--;
						}

						i++;
					} while (loop_scope > 0);
				}
				break;
			case CMD_LOOP_END: // If the byte at the data pointer is nonzero, jump to the corresponding brace [
				if (cells[cell_ptr] != 0)
				{
					do
					{
						if (program[i] == CMD_LOOP_END)
						{
							loop_scope++;
						}
						else if (program[i] == CMD_LOOP_BEG)
						{
							loop_scope--;
						}
						i--;
					} while (loop_scope > 0);
				}
				break;
			default:
				break;
		};
	}

	return 0;
}
